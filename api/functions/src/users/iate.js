const functions = require("firebase-functions");
const admin = require("firebase-admin");
const Joi = require("joi");
const moment = require("moment");

export const translator = {

  async get(req, res, next) {
    try {
      const id = req.params.id;
      const result = await interactor.get(id);
      res.json(result);
    } catch (err) {
      res.status(400).json(err);
    }
    return next();
  },

  async validatePutData(data) {
    const schema = Joi.object({
      wallet: Joi.string().length(42),
      updated: Joi.date().default(moment().toISOString()).iso().forbidden(),
    });

    const { error, value } = Joi.validate(data, schema);
    if(!error) return value;

    let messages = error.details.map(e => e.message);

    throw new Error(messages.toString());
  },

  async put(req, res, next) {
    try {
      const id = req.params.id;
      const data = req.body;
      const body = await translator.validatePutData(data);
      const result = await interactor.save(id, body);
      res.status(200).json(result);
    } catch (err) {
      res.status(400).json(err);
    }
    return next();
  },
};

export const interactor = {

  async get(id) {
    return await entity.get(id);
  },

  async save(id, body) {
    return await entity.save(id, body);
  },

};

export const entity = {

  async get(id) {
    return await adapter.get(id);
  },

  async save(id, data) {
    return await adapter.save(id, data);
  },

};

const adapter = {

  async get(id) {
    const result = await admin.firestore().collection("/users").doc(id).get();
    console.log(result.id);
    return result.exists ? {
      id: result.id,
      ...result.data()
    } : {
      id: id,
      wallet: ""
    };
  },
    
  async save(id, data) {
    const result = await admin.firestore().collection("/users").doc(id).set(data, { merge: true });
    return { id: result.id, ...data };
  },

};