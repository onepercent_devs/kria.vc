const express = require("express");
const cors = require("cors");
const users = require("./users/iate");

/************ SERVER CONFIGS ************/
const app = express();
app.use(cors({
  origin: true
}));

/** ALL TRANSLATORS GO HERE */
function getUser(req, res, next) {
  return users.translator.get(req, res, next);
}
function putUser(req, res, next) {
  return users.translator.put(req, res, next);
}

/** ALL VERSION UNDER MAINTENANCE WILL BE KEPT HERE **/
const v1 = express.Router();

/************ ROUTES V-LATEST ************/
app.options("*", cors());

/**************** USERS *****/
v1.use("/users", express.Router()
  .get("/:id", getUser));

v1.use("/users", express.Router()
  .put(":id", putUser));

app.use("/v1", v1);
app.use("/", v1);
/****************************************/

module.exports = app;