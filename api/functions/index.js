// CONST
const functions = require("firebase-functions");
const admin = require("firebase-admin");

// INITIALIZATIONS
admin.initializeApp();

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions
const server = require("./dist/server");
exports.api = functions.https.onRequest(server);
