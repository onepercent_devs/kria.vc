# kria.vc script 


## Mint tokens to investors according the CSV(RFC 4180) file.

### Usage: 

```
npx kria.vc -f invest.csv -n mainnet

Options:
  -f, --file      The file to be loaded. Ex. /tmp/investors.csv                              [required]
  -n, --network   The network [mainnet, ropsten]                                             [required]
  -c, --contract  SmartContract address to be called. Ex. 0x154ab422aec31987caf72563na4dAB1  [required]
```