// COMMAND LINE VALIDATIONS
var argv = require('optimist')
  .usage('Mint tokens to investors according the CSV(RFC 4180) file.\n\nUsage: kria.vc -f invest.csv -n mainnet')
  .demand(['f', 'n', 'c'])
  .alias('f', 'file')
  .describe('f', 'The file to be loaded. Ex. /tmp/investors.csv')
  .alias('n', 'network')
  .describe('n', 'The network [mainnet, ropsten]')
  .alias('c', 'contract')
  .describe('c', 'SmartContract address to be called. Ex. 0x154ab422aec31987caf72563na4dAB1')
  .argv
;

// CONST
const INFURA_API_KEY = "CSFzT8ExhswKonwYIA3n";

// BLOCKCHAIN STUFF
var Web3 = require('web3');
var provider = 'https://' + argv.network + '.infura.io/' + INFURA_API_KEY;
var web3 = new Web3(new Web3.providers.HttpProvider(provider));
web3.eth.getBlockNumber().then(function(b) {
  console.info('Highest block: ', b);
}).catch(function(e) {
  console.error('Error, check your network name or network status', e);
})


// FILE TREATMENT
var fs = require('fs');
var es = require('event-stream');
var s = fs.createReadStream(argv.file);
console.info('\nLoading file ...')
s
  .pipe(es.split())
  .pipe(es.mapSync(function(l) {
    s.pause();
      l.length ? console.log(transformCSV(l)) : null;
      // call contract 
    s.resume();
  }))
  .on('error', function(e) {
    console.error('Error reading file ', e);
  })
  .on('end', function() {
    console.info('File loaded!')
  })
;

// PURE FUNCTIONS
function transformCSV(l) {
  var o = {};
  l
    .split(',')
    .map(function(d, i) {
      switch(i) {
        case 0: // CPF/CNPJ
          o.sn = d;
        break;        
        case 1: // AMOUNT
          o.v = Number(d).toFixed(2);
        break;
        case 2: // WALLET
          o.w = { address: d };
        break;
        default:
        break;
      }
    })
  if(!o.w) o.w = web3.eth.accounts.create();
  return o;
}