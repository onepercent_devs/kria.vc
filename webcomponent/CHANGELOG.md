#### 1.1.2 (2018-08-16)

##### Chores

* **scripts:**  Testing (3d7498a2)

##### Other Changes

* **other:**  hooks improvement (2a43d79b)

#### 1.1.2 (2018-08-16)

##### Other Changes

* **other:**  hooks improvement (2a43d79b)

#### 1.1.1 (2018-08-16)

##### Other Changes

* **other:**  hooks improvement (2a43d79b)

### 1.1.0 (2018-08-16)

##### Other Changes

* **other:**  hooks improvement (2a43d79b)

#### 1.0.1 (2018-08-16)

##### Other Changes

* **other:**  hooks improvement (2a43d79b)

