import { h, render } from "preact";
import { RouteApp } from "./logic/routes";
import tracker from "./tracker";
if (process.env.NODE_ENV === "development") {
  require("preact/devtools");
}

tracker.setUp();

const internalRender = (options, el, merge) => render(<RouteApp {...options} />, el, merge);

const KriaWebC = {

  init: (options) => {
    console.log("kriawebc-sdk-version:", process.env.SDK_VERSION);

    tracker.track();

    const kriaRoot = document.getElementById("kria-webc-root");
    const element = internalRender(options, kriaRoot);

    return {
      element,
      options,

      setOptions(opt) {
        this.options = {...this.options, ...opt};
        internalRender(this.options, kriaRoot, this.element);
        return this.options;
      },

      unmount() {
        render(null, kriaRoot, this.element);
      }
    };
  }

};

export default KriaWebC;