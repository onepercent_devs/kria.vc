import { h } from "preact";
import classNames from "classnames";

import Spinner from "../Spinner";

import style from "./style.css";

const Loading = (props) => {

  return (
    <div className={classNames(style.loading, { [style.show]: props.isLoading })}>
      <div className="d-flex h-100 flex-column align-items-center justify-content-center">
        <Spinner />
      </div>
    </div>
  );

};

export default Loading;