import AssetSpinner from "./Spinner";
import AssetLoading from "./Loading";

export default {
  AssetSpinner,
  AssetLoading
};