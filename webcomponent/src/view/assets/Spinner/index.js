import { h } from "preact";
import style from "./style.css";

const Spinner = () => {
  return (
    <div className={style.spinner}>
      <div></div>
      <div></div>
    </div>
  );
};

export default Spinner;