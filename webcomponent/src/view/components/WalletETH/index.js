import { h, Component } from "preact";

import { AssetLoading as Loading } from "../../assets";

class ViewWalletETH extends Component {

  constructor(props) {
    super(props);
  }

  render () {
    const {
      readOnly,
      wallet,
      onChangeField,
      onSaveWallet,
      onCreate,
      isLoading,
      i18n
    } = this.props;
    return (
      <div className="col-md-6">
        <div className="card mb-2 shadow-sm">
          <div className="card-body">
            <p className="card-text">{i18n.t("wallet.title")}</p>
            <form>
              <div>
                <input ref={r => this.input = r} type="text" className="form-control form-control-sm" id="wallet" placeholder={i18n.t("wallet.placeholder")} value={wallet} disabled={readOnly} onChange={e => onChangeField({ wallet: e.target.value })}/>
                <div className="d-flex justify-content-between align-items-center">
                  { readOnly ?
                    <small className="text-muted">{i18n.t("wallet.message_filled")}</small>
                    :
                    <small className="text-muted">{i18n.t("wallet.message_not_filled")}</small>
                  }
                </div>
                <hr/>
                <div className="d-flex flex-row justify-content-end align-items-center">
                  <a href="https://kb.myetherwallet.com/getting-started/creating-a-new-wallet-on-myetherwallet.html" className="btn btn-outline-secondary btn-sm" onClick={onCreate}>{i18n.t("wallet.button_create")}</a>
                  <button className="btn btn-primary btn-sm ml-1" type="button" disabled={readOnly} onClick={onSaveWallet}>{i18n.t("wallet.button_save")}</button>
                </div>
              </div>
            </form>
          </div>
          <Loading isLoading={ isLoading } />
        </div>
      </div>
    );
  }

}

export default ViewWalletETH;