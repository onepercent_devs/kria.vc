import RouteWalletETH from "./WalletETH";
import RouteApp from "./App";

export default {
  RouteApp,
  RouteWalletETH
};