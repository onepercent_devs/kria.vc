import { h, Component } from "preact";
import { connect } from "react-redux";
import swal from "sweetalert";

import { edit } from "../reducers/walletETH/edit";
import { get } from "../reducers/walletETH/get";
import { ViewWalletETH } from "../../view/components";
import { eth } from "../utils";
import { sendEvent, sendError } from "../../tracker";
import { isLoading } from "../selectors/walletETH";

import * as C from "../constants";

const mapStateToProps = state => ({
  walletETHGet: state.walletETHGet,
  walletETHEdit: state.walletETHEdit,
  isLoading: isLoading(state),
});

const mapDispatchToProps = dispatch => ({
  edit: data => dispatch(edit(data)),
  get: id => dispatch(get({ id }))
});

class RouteWalletETH extends Component {

  constructor(props) {
    super(props);
    
    this.handleSaveWallet = this.handleSaveWallet.bind(this);
    this.handleClickCreate = this.handleClickCreate.bind(this);
    
    this.state = {
      status: C.STATUS_INIT,
      wallet: ""
    };
  }

  componentDidMount() {
    this.props.get(this.props.id);
  }

  componentDidUpdate(prevProps) {
    // any request happens
    if (prevProps.isLoading && !this.props.isLoading) {

      // get happens?
      if (prevProps.walletETHGet.status !== this.props.walletETHGet.status) {
        switch (this.props.walletETHGet.status) {
          case C.STATUS_OK:
            this.setState({
              wallet: this.props.walletETHGet.data.wallet
            });
            break;
          case C.STATUS_FAIL:
            break;
        }
      }

      // edit happens?
      if (prevProps.walletETHEdit.status !== this.props.walletETHEdit.status) {
        switch (this.props.walletETHEdit.status) {
          case C.STATUS_OK:
            swal("Success", this.props.i18n.t("wallet.success"), "success");
            break;
          case C.STATUS_FAIL:
            if(this.view) this.view.input.focus();
            swal("Oops", this.props.i18n.t("wallet.errors.save_fail"), "error")
              .then(() => this.view && this.view.input.focus());
            break;
        }
      }

    }
  }

  handleSaveWallet() {
    if (!eth.isValid(this.state.wallet)) {
      sendError("User input wrong wallet address");
      swal("Oops", this.props.i18n.t("wallet.errors.invalid_wallet"), "error")
        .then(() => this.view && this.view.input.focus());
      return;
    }
    this.props.edit({
      id: this.props.id,
      wallet: this.state.wallet
    });
  }

  handleClickCreate() {
    sendEvent("Looking for help to create wallet");
  }

  render() {
    const {
      i18n,
      readOnly
    } = this.props;

    return (
      <ViewWalletETH
        ref={r => this.view = r}
        onChangeField={e => {
          this.setState({ ...e });
        }}
        onSaveWallet={this.handleSaveWallet}
        onCreate={this.handleClickCreate}
        wallet={this.state.wallet}
        readOnly={readOnly}
        isLoading={this.props.isLoading}
        i18n={i18n}
      />
    );
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(RouteWalletETH);