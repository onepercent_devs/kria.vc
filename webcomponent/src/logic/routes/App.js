import { h, Component } from "preact";
import { Provider } from "react-redux";

import mountStore from "../store";
import RouteWalletETH from "./WalletETH";
import { initializeI18n } from "../../view/locales";

class RouteApp extends Component {

  constructor(props) {
    super(props);
    
    this.store = mountStore(this.props.hooks);

    this.state = {
      i18n: initializeI18n(this.props.language),
      externalUserId: this.props.externalUserId
    };
  }

  hasChanged(actual, next, prop) {
    return actual[prop] !== next[prop];
  }

  hasChangedUser(props, nextProps) {
    return this.hasChanged(props, nextProps, "externalUserId");
  }

  hasChangedComponent(props, nextProps) {
    return this.hasChanged(props, nextProps, "component");
  }

  hasChangedLanguage(props, nextProps) {
    return this.hasChanged(props, nextProps, "language");
  }

  componentWillReceiveProps(nextProps) {
    if (this.hasChangedLanguage(this.props, nextProps))
      this.setState({ i18n: initializeI18n(nextProps.language) });
  }

  render () {
    return (
      <div className="root">
        <Provider store={this.store}>
          { this.props.component === "wallet" &&
            <RouteWalletETH
              i18n={this.state.i18n}
              id={this.props.externalUserId}
              readOnly={this.props.readOnly}
            />
          }
        </Provider>
      </div>
    );
  }

}

RouteApp.defaultProps = {
  language: "en",
  component: "wallet",
  externalUserId: "",
  readOnly: true,
  hooks: []
};

export default RouteApp;