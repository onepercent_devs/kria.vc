const SHA3 = require("crypto-js/sha3");

const sha3 = (value) => {
  return SHA3(value, {
    outputLength: 256
  }).toString();
};

const isChecksumAddress = (address) => {
  address = address.replace("0x", "");
  let addressHash = sha3(address.toLowerCase());
  for (let i = 0; i < 40; i++ ) {
    if ((parseInt(addressHash[i], 16) > 7 && address[i].toUpperCase() !== address[i]) ||
        (parseInt(addressHash[i], 16) <= 7 && address[i].toLowerCase() !== address[i])) {
      return false;
    }
  }
  return true;
};

const isValid = (address) => {
  if (!/^(0x)?[0-9a-f]{40}$/i.test(address)) return false;
  return isChecksumAddress(address);
};

module.exports.isValid = isValid;