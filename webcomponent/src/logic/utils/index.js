import eth from "./eth";
import * as array from "./array";

export default {
  eth,
  array
};