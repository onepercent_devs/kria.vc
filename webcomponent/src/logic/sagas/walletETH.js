import { put, call, takeLatest } from "redux-saga/effects";

import { walletGet, walletPut } from "../apis";
import * as C from "../constants";
import {
  getOk,
  getFail
} from "../reducers/walletETH/get";
import {
  editOk,
  editFail
} from "../reducers/walletETH/edit";

function* getSaga(action) {
  try {
    const result = yield call(walletGet, action.payload);
    yield put(getOk(result));
  } catch (err) {
    yield put(getFail(err));
  }
}

function* editSaga(action) {
  try {
    yield call(walletPut, action.payload);
    yield put(editOk());
  } catch (err) {
    yield put(editFail(err));
  }
}

export default [
  takeLatest(C.WALLET_ETH_GET, getSaga),
  takeLatest(C.WALLET_ETH_EDIT, editSaga)
];