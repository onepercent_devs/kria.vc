import { all } from "redux-saga/effects";

import sagaHooks from "./hooks";
import sagaWalletETH from "./walletETH";

function* mySaga() {
  try {
    yield all([
      ...sagaHooks,
      ...sagaWalletETH
    ]);
  } catch(e) {
    console.log("Saga error: ", e);
  }
}

export default mySaga;