export const STATUS_INIT = "INIT";
export const STATUS_OK = "OK";
export const STATUS_FAIL = "FAIL";
export const STATUS_WAITING = "WAITING";

export const WALLET_ETH_EDIT = "ETH_EDIT";
export const WALLET_ETH_EDIT_OK = "ETH_EDIT_OK";
export const WALLET_ETH_EDIT_FAIL = "ETH_EDIT_FAIL";

export const WALLET_ETH_GET = "ETH_GET";
export const WALLET_ETH_GET_OK = "ETH_GET_OK";
export const WALLET_ETH_GET_FAIL = "ETH_GET_FAIL";